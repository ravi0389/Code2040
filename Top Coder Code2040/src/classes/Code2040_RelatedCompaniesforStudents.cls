public class Code2040_RelatedCompaniesforStudents{
public string wrapperjson{get;set;}


        public Code2040_RelatedCompaniesforStudents(ApexPages.StandardController std)
        {
            map<String,string> studentsSkilllevel=new map<string,string>();
            Student_Detail__c student=(Student_Detail__c)std.getRecord();
            List<Skill__c> skillSet=[select name,Skill_Level__c from Skill__c where Student__c=:ApexPages.currentPage().getParameters().get('id')];
            set<String> skillnames=new set<string>();
            for(Skill__c s:skillset)
            {
                    skillnames.add(s.name);
                    studentsSkilllevel.put(s.name,s.Skill_Level__c);
            }
            List<Skill__c> skillList=[select id,Name,Skill_Level__c,Company__r.Name from Skill__c where Company__C!=NULL AND Name IN :skillnames 
                                                            AND Company__r.does_the_intern_have_any_prior_work_expe__c=:student.does_the_intern_have_any_prior_work_exp__c AND
                                                            Company__r.is_the_intern_returning_to_school_after__c=:student.is_the_intern_returning_to_school_after__c];
                                                            
            Map<String,List<Skill__c>> myMap=new Map<String,List<Skill__c>>();
            for(Skill__c s:skillList)
            {
            if(studentsSkilllevel.get(s.name)==s.Skill_Level__c)
            {
                  List<Skill__c> tempList=new List<Skill__c>();
                if(myMap.containskey(s.Name))
                {
                   tempList=myMap.get(s.name);
                   tempList.add(s);
                }
               else     
                   tempList.add(s);
                    
                    myMap.put(s.name,tempList);
                    }
             }
           List<wrapperclass> wrapperList=new List<wrapperClass>();
           for(String s:myMap.keyset())
           {
                   wrapperclass wp=new wrapperclass();
                   wp.languageName=s;
                   wp.myCompanies=myMap.get(s);
                   wrapperList.add(wp);
           }
           wrapperjson=Json.serialize(wrapperList);
        }
        public class wrapperClass
        {
                public List<Skill__c> myCompanies;
                public String languageName;
        }

}