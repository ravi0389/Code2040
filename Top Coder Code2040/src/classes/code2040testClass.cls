@istest
public class code2040testClass
{
    public static testmethod void CompaniesandStudentsSkillPage()
    {
        //inserting student record
        Company_Details__c c=new Company_Details__c();
        c.name='Company2';
        insert c;
        
        // inserting company record
        Student_Detail__c s=new Student_Detail__c();
        s.name='Student1';
        insert s;
        
        // insert skill record
        Skill__c s1=new Skill__c();
        s1.Company__c=c.id;
        s1.Name='Java';
        s1.Skill_Level__c='Expert';
        insert s1;
        
        Skill__c s2=new Skill__c();
        s2.Student__c=s.id;
        s2.Name='Java';
        s2.Skill_Level__c='Expert';
        insert s2;
        
         ApexPages.StandardController sc = new ApexPages.StandardController(c);
         CompanyExtension comp=new CompanyExtension(sc);
          comp.getweightageMap();
         comp.doSave(); 
         
         ApexPages.StandardController sc1 = new ApexPages.StandardController(s);
         StudentExtension student=new StudentExtension(sc1);
         student.getweightageMap();
         student.doSave();
         Code2040_RelatedCompaniesforStudents relatedComp=new Code2040_RelatedCompaniesforStudents(sc1);
         
        
    }
    
   
    
}