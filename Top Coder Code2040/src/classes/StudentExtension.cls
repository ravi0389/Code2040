public class StudentExtension
{

public string skillJson{Get;set;}
public ApexPages.StandardController ctrl;
    
    public StudentExtension(ApexPages.StandardController std)
    {
              list<Skill__c> skilllist=[select id,Name,Skill_Level__c from  Skill__c where Student__c=:ApexPages.currentPage().getParameters().get('id')];
               skillJson= JSON.serialize(skilllist);
    }
    
    public pagereference doSave()
    {
          List<Skill__c> skillist= 
          (List<Skill__c>)JSON.deserialize(skillJson, List<Skill__c>.class);
          for(Skill__c s:skillist)
          {
                 s.Student__c=ApexPages.currentPage().getParameters().get('id');
          }
          upsert skillist;
          return NULL;
    }
    
    public string  getweightageMap()
    {
            Map<String,string> myMap=new Map<String,string>();
            mymap.put('None','0%');
            mymap.put('Some experience','45%');
            mymap.put('Heavy experience','70%');
            mymap.put('Expert','100%');
            return JSON.serialize(myMap);
    
    }
}